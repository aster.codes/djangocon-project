from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Question
from django.views.generic import ListView
from django.views.generic import DetailView


class QuestionListView(ListView):
    template_name = 'polls/index.html'

    def get_queryset(self):
        return Question.objects.all()


def question_list_view(request):
    results = Question.objects.all()
    return render(request, 'polls/index.html', {'question_list':
                                                results})


class QuestionDetailView(DetailView):
    model = Question
    template_name = 'polls/detail.html'


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})
